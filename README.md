Tepl - Text editor product line
===============================

Tepl is a library that eases the development of GtkSourceView-based text
editors and IDEs.

Tepl was previously named Gtef (GTK text editor framework). The project has
been renamed in June 2017 to have a more beautiful name. The end of Tepl is
pronounced like in “apple”.

More information
----------------

Tepl currently targets **GTK 3**.

[More information about Tepl](docs/more-information.md).
