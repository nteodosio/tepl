Source: tepl
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Tanguy Ortolo <tanguy+debian@ortolo.eu>, @GNOME_TEAM@
Build-Depends: at-spi2-core <!nocheck>,
               dbus <!nocheck>,
               debhelper-compat (= 12),
               gnome-pkg-tools,
               gobject-introspection (>= 1.42.0),
               gtk-doc-tools (>= 1.25),
               gvfs <!nocheck>,
               libamtk-5-dev (>= 5.0),
               libgirepository1.0-dev (>= 1.42.0),
               libglib2.0-dev (>= 2.64),
               libglib2.0-doc,
               libgtk-3-dev (>= 3.22),
               libgtk-3-doc,
               libgtksourceview-4-dev (>= 4.0),
               libgtksourceview-4-doc,
               libicu-dev,
               meson (>= 0.53),
               pkg-config,
               xauth <!nocheck>,
               xvfb <!nocheck>,
Standards-Version: 4.5.0
X-Ubuntu-Use-Langpack: yes
Homepage: https://wiki.gnome.org/Projects/Tepl
Vcs-Browser: https://salsa.debian.org/gnome-team/tepl
Vcs-Git: https://salsa.debian.org/gnome-team/tepl.git
Rules-Requires-Root: no

Package: libtepl-5-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-tepl-5 (= ${binary:Version}),
         libtepl-5-0 (= ${binary:Version}),
         libamtk-5-dev (>= 5.0),
         libglib2.0-dev (>= 2.64),
         libgtk-3-dev (>= 3.22),
         libgtksourceview-4-dev (>= 4.0),
         ${misc:Depends}
Description: Text editor library for GTK - development files
 Tepl is a library that eases the development of GtkSourceView-based text
 editors and IDEs. Tepl is the acronym for “Text editor product line”. It
 serves as an incubator for GtkSourceView.
 .
 This package contains the development headers and libraries.

Package: libtepl-5-0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Text editor library for GTK
 Tepl is a library that eases the development of GtkSourceView-based text
 editors and IDEs. Tepl is the acronym for “Text editor product line”. It
 serves as an incubator for GtkSourceView.

Package: gir1.2-tepl-5
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${misc:Depends}
Description: Text editor library for GTK - GObject introspection
 Tepl is a library that eases the development of GtkSourceView-based text
 editors and IDEs. Tepl is the acronym for “Text editor product line”. It
 serves as an incubator for GtkSourceView.
 .
 This package contains the GObject introspection binding.
