Tepl - more information
=======================

About versions
--------------

Tepl follows the even/odd minor version scheme.

For example the `4.1.x` versions are unstable (development versions), and the
`4.2.x` versions are stable.

Dependencies
------------

- GLib
- GTK 3
- GtkSourceView 4
- [Amtk](https://gitlab.gnome.org/swilmet/amtk/) 5
- [ICU](http://site.icu-project.org/)

Documentation
-------------

See the `gtk_doc` Meson option. A convenient way to read the API documentation
is with the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) application.

See also other files in this directory for additional notes.

Some links
----------

- [Project home page](https://gitlab.gnome.org/swilmet/tepl)
- [Tarballs](https://download.gnome.org/sources/tepl/)
- [Old Gtef tarballs](https://download.gnome.org/sources/gtef/)

Development and maintenance
---------------------------

The project is in low-development / maintenance state.
